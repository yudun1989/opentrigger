#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
from model import Model
from model.user_task import UserTask

class User(Model):
    def get_tasks(self):
        tasks = UserTask.where(user_id = self.id)
        return tasks

