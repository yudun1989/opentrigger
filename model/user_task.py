#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
import init_env
from model import Model
from channel_task import ChannelTask
class UserTask(Model):
    @property
    def from_task(self):
        return ChannelTask.get(self.channel_task_id)

    @property
    def to_task(self):
        return ChannelTask.get(self.channel_task_id2)
