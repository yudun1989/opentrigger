#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
import init_env
from model import Model
channel_pic_url = '/static/pic/channels/'
channel_url = '/channels/'
reactive_url = '/reactive/'

class Channel(Model):
    def pic_url(self, switch = 'on',size = 110 ):
        return channel_pic_url+self.name+'_'+switch+str(size)+'.png'

    def url(self):
        return channel_url + str(self.id)

    def reactive_url(self):
        return reactive_url + self.name