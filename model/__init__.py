#!/usr/bin/env python
#coding:utf-8
import init_env
__author__ = 'liyudong'
import sys
from settings import DBNAME, DBUSER ,DBPASSWORD, DBENGINE
DB_HOST_ONLINE = "127.0.0.1:%s:%s:%s:%s"%(3306,DBNAME, DBUSER, DBPASSWORD)
DATABASE_CONFIG = {
    "opentrigger":{
        "master": DB_HOST_ONLINE,
        "tables": (
            "channel",
            "channel_info",
            "user",
            "*",
            )
    },
    }
THREAD_SAFE = False
from sqlbean.db import connection
connection.THREAD_SAFE = THREAD_SAFE

from sqlbean.db import sqlstore
SQLSTORE = sqlstore.SqlStore(db_config=DATABASE_CONFIG, charset="utf8")
def get_db_by_table(table_name):
    return SQLSTORE.get_db_by_table(table_name)
connection.get_db_by_table = get_db_by_table

from sqlbean.shortcut import Model
