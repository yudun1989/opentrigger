#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
import init_env
from model import Model
class ChannelTask(Model):
    @classmethod
    def from_tasks(cls):
        return cls.where(fromto=1)

    @classmethod
    def to_tasks(cls):
        return cls.where(fromto=0)