#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
import web
from apps.home import HomePage
from apps.admin.admin import AdminHome
from apps.infoc import InfoCenter
from apps.channels import Channels, ChannelItem
from apps.reactive import *
from apps.auth import Login,Logout
from settings import DEBUG
from apps.task import Task

urls = [
    '/','HomePage',
    '/admin','AdminHome',
    '/infoc','InfoCenter',
    '/channels','Channels',
    '/channels/(\d+)','ChannelItem',
    '/login','Login',
    '/logout','Logout',
    '/tasks','Task'
    ]

for key,value in REACTIVE_APP_MAP.items():
    urls.append('/reactive/'+key)
    urls.append(value)

urls = (urls)

app = web.application(urls, globals(), autoreload=True)

if __name__ == "__main__":
    if DEBUG == 'off':
        web.wsgi.runwsgi = lambda func, addr=None: web.wsgi.runfcgi(func, addr)
    app.run()
