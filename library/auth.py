# coding=utf-8
import string
import hmac #hmac的用法 http://zhuoqiang.me/a/password-storage-and-python-example
import web
import time, datetime
from model.user import User
import init_env

SECRET = 'opentrigger'
COOKIE_NAME = 'opentrigger_cookie'


def login_required(func):
    def Function(*args):
        user = get_user()
        if user:
            return func(*args)
        else:
            web.seeother('/login')
    return Function

def setcookie(user, remember = False):#给用户设置cookie，默认不保存
    text = cookie(user)
    web.setcookie(COOKIE_NAME, text)

def logout():
    web.setcookie(COOKIE_NAME, '') #logout即为将cookie清空

def h(*l):
    return hmac.new(SECRET, repr(l)).hexdigest()   #repr为将obj转换为str    eval(repr(object))=object 这个牛逼

def cookie(user):
    t = datetime.datetime(*time.gmtime()[:6]).isoformat()
    return '%s/%s/%s' % (h(user.id, user.password), user.id, str(t))

def get_user():
    #从cookie里面获得用户信息
    session = web.cookies(opentrigger_cookie=None).opentrigger_cookie
    if session:
        digest, userid, time = session.split('/')
        real_user = User.get(userid)
        if digest == h(real_user.id, real_user.password):
            return real_user
