#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'

from web.contrib.template import render_mako
render = render_mako(
    directories=['template','template/reactive'],
    input_encoding = 'utf-8',
    output_encoding = 'utf-8',
)