#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
import init_env
from configure import render
from model.channel import Channel

class InfoCenter:
    def GET(self):
        return render.infoc()