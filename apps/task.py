#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
from configure import render
from model.channel import Channel
from model.channel_info import ChannelInfo
from library.auth import login_required, get_user
from model.user_task import UserTask
import web

class Task:
    @login_required
    def GET(self):
        real_user = get_user()
        tasks = real_user.get_tasks()
        return render.tasks(tasks = tasks)

    @login_required
    def POST(self):
        real_user = get_user()
        tasks = real_user.get_tasks()
        data = web.input()
        remove_task = data.get("remove")
        from_task = data.get("from_task")
        to_task = data.get("to_task")
        if from_task and to_task:
            task = UserTask(channel_task_id = int(from_task),channel_task_id2 = int(to_task),user_id = real_user.id)
            task.save()
        elif remove_task:
            remove = UserTask.get(int(remove_task))
            remove.delete()
        else:
            pass
        return render.tasks(tasks = tasks)

