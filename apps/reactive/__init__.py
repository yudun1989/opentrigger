#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
from weather import Weather
from sina import Sina
from sms import Sms
from douban import Douban


REACTIVE_APP_MAP = {
    'gmail':'Gmail',
    'weather':'Weather',
    'sina':'Sina',
    'douban':'Douban',
    'sms':'Sms'
}