#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
import init_env
from configure import render
from model.channel import Channel
from library.auth import login_required, get_user
from model.channel_info import ChannelInfo
import web
import urllib2, urllib
from BeautifulSoup import BeautifulSoup


class Sms:
    @login_required
    def GET(self):
        sms_channel = Channel.get(name='sms')
        real_user = get_user()
        result = ChannelInfo.get(channel_id = sms_channel.id, user_id = real_user.id)
        return render.sms(channel = sms_channel, result = result)

    @login_required
    def POST(self):
        sms_channel = Channel.get(name='sms')
        real_user = get_user()
        result = ChannelInfo.get(channel_id = sms_channel.id, user_id = real_user.id)
        data = web.input()
        sms_number = data.get("sms_number")
        if result:
            result.info = sms_number
            result.save()
        else:
            result = ChannelInfo(channel_id = sms_channel.id, user_id = real_user.id, info = sms_number)
            result.save()
        return render.weather(channel = sms_channel, result = result)