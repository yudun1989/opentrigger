#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'

import init_env
from configure import render
from model.channel import Channel
from library.auth import login_required, get_user
from model.channel_info import ChannelInfo
import web
import urllib2, urllib
from BeautifulSoup import BeautifulSoup

class Weather:
    @login_required
    def GET(self):
        weather_channel = Channel.get(name='weather')
        real_user = get_user()
        result = ChannelInfo.get(channel_id = weather_channel.id, user_id = real_user.id)
        return render.weather(channel = weather_channel, result = result)

    @login_required
    def POST(self):
        weather_channel = Channel.get(name='weather')
        real_user = get_user()
        result = ChannelInfo.get(channel_id = weather_channel.id, user_id = real_user.id)
        data = web.input()
        city_name = data.get("city_name")
        if result:
            result.info = city_name
            result.save()
        else:
            result = ChannelInfo(channel_id = weather_channel.id, user_id = real_user.id, info = city_name)
            result.save()
        return render.weather(channel = weather_channel, result = result)


def to_str(str):
    result_string = unicode(str,"gbk")
    result_string = result_string.encode("utf-8")
    return result_string



def get_weather(city):
    my_str = ''
    url = 'http://www.google.com/ig/api?hl=zh-cn&weather='+city
    res = urllib2.urlopen(url).read()
    res = to_str(res)
    soup = BeautifulSoup(res)
    #print soup.prettify()
    root = soup.contents[1].contents[0]
    if root.problem_cause:
        return city+' has no information'
    city = root.contents[0].city['data']
    forecast_date = root.contents[0].forecast_date['data']

    my_str = my_str +'city:'+city +'\n'
    my_str = my_str+ 'Today'+'\n'
    my_str = my_str+ 'forecast_date:'+forecast_date+'\n'

    current_condition = root.contents[1].condition['data']
    temperature = root.contents[1].temp_c['data']
    wind_condition = root.contents[1].wind_condition['data']

    my_str = my_str + 'current_condition:' + current_condition+'\n'
    my_str = my_str+ 'temperature:' + temperature+'\n'
    my_str = my_str+ 'wind_conditon:' + wind_condition+'\n\n'

    for i in xrange(4):
        my_str = my_str+ root.contents[i+2].day_of_week['data']+'\n'
        my_str = my_str+ root.contents[i+2].condition['data']+'\n'
        my_str = my_str+ 'high:'+root.contents[i+2].high['data']+'\n'
        my_str = my_str+ 'low:'+root.contents[i+2].low['data']+'\n'
    return my_str

