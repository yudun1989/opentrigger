#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'

import init_env
from configure import render
from model.channel import Channel
from library.auth import login_required, get_user
from model.channel_info import ChannelInfo
from library.oauth.weibo import APIClient
from settings import DOMAIN_NAME
import web
import json



class Douban:
    @login_required
    def GET(self):
        user = get_user()
        sina_channel = Channel.get(name='sina')
        client = APIClient(app_key=sina_channel.key,app_secret=sina_channel.secret,redirect_uri='http://'+DOMAIN_NAME+'/reactive/sina')
        user_data = web.input()
        if not user_data.has_key('code'):
            web.seeother(client.get_authorize_url())
        else:
            client = APIClient(app_key=sina_channel.key,app_secret=sina_channel.secret,redirect_uri='http://'+DOMAIN_NAME+'/reactive/sina')
            code = user_data.get('code')
            r = client.request_access_token(code)
            access_token = r.access_token
            expires_in = r.expires_in
            channel_info = ChannelInfo(channel_id = sina_channel.id, user_id = user.id, info = access_token+':'+str(expires_in))
            channel_info.save()
            return render.sina(result = channel_info, channel = sina_channel)

    @login_required
    def POST(self):
        pass
