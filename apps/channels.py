#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
from configure import render
from model.channel import Channel
from model.channel_info import ChannelInfo
from library.auth import login_required, get_user
import web


class Channels:
    def GET(self):
        channels = Channel.where()
        return render.channels(channels = channels)

class ChannelItem():
    def GET(self,item_id):
        user = get_user()
        channel = Channel.get(item_id)
        channel_info = ChannelInfo.get(user_id = user and user.id or None, channel_id = channel.id)
        if channel:
            return render.channel_item(channel = channel,channel_info = channel_info)
        else:
            raise web.seeother('/')