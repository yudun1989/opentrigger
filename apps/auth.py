#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
from configure import render
from model.user import User
from library.auth import h,setcookie ,get_user, login_required, logout
import web

class Login:
    def GET(self):
        real_user = get_user()
        if real_user:
            return web.seeother('/')
        else:
            return render.login()

    def POST(self):
        data = web.input()
        mail = data.get("mail")
        password = data.get("password")
        password = h(password)
        user =  User.get(mail = mail,password = password)
        if user:
            setcookie(user)
            web.seeother('/')
        else:
            return
class Logout:
    def GET(self):
        logout()
        web.seeother('.')
