#!/usr/bin/env python
#coding:utf-8
__author__ = 'liyudong'
import init_env
from configure import render
from model.channel import Channel

class HomePage:
    def GET(self):
        channels = Channel.where()
        loc_vars = locals()
        del loc_vars['self']
        return render.homepage(channel = channels)